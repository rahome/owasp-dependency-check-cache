#!/bin/sh
NVD_API_KEY="__NVD_API_KEY__"

/usr/share/dependency-check/bin/dependency-check.sh --version
/usr/share/dependency-check/bin/dependency-check.sh --updateonly --nvdApiKey "'${NVD_API_KEY}'" --nvdApiDelay 6000
