# owasp-dependency-check-cache

> [OWASP dependency-check](https://github.com/dependency-check/DependencyCheck) is a software composition analysis
> utility that detects publicly disclosed vulnerabilities in application dependencies.

The purpose of this project is to improve pipeline performance when scanning for vulnerabilities in application
dependencies by providing a prebuilt Docker container where the vulnerability database is already downloaded.

The Docker container is rebuilt automatically on a schedule which will also update the vulnerability database, you can
view the schedule configuration in
the [Schedules](https://gitlab.com/rahome/owasp-dependency-check-cache/-/pipeline_schedules) view.

## Use with GitLab CI

If you are using GitLab CI to build your CI/CD pipelines, you can include
the [`Owasp.gitlab-ci.yml`](Owasp.gitlab-ci.yml) job template which exposes jobs to scan the project for vulnerabilities
in the application dependencies.

### Include job template

How you include the job template is depending on whether you use GitLab.com or a self-hosted instance.

#### Using GitLab.com

If you are using GitLab.com you can use the `project:` directive since both projects are hosted on GitLab.com.

```yaml
include:
  - project: 'rahome/owasp-dependency-check-cache'
    ref: main
    file: '/Owasp.gitlab-ci.yml'
```

#### Using self-hosted instance

If you use a self-hosted instance you need to use the `remote:` directive since the project can not be resolved using a
relative path.

```yaml
include:
  - remote: 'https://gitlab.com/rahome/rahome/owasp-dependency-check-cache/-/raw/main/Owasp.gitlab-ci.yml'
```

If you want to use the `project:` directive on a self-hosted instance, you can configure a repository mirror from this
project to a project on your self-hosted instance.

However, it is important to know that you will still use the image from this project as the `image:` directive include
the container registry path (i.e. `registry.gitlab.com`). This is required to prevent supply chain attacks, i.e. where
another registry supply a malicious image with the same name.

#### Version considerations

Since we are unable to track who is using the template, and we might need to do breaking changes, it is recommended to
not use a branch reference (i.e. `main` in this case) instead you should use a git commit hash.

It might make it a bit harder to keep up-to-date, but your pipelines will be better isolated from changes.

### Continuous scanning

The `owasp-dependency-check-cache` container is recreated, with a fresh database, once per day at 02:00 UTC via a
scheduled GitLab CI job. In order to enable continuous scanning it's recommended to schedule jobs after 03:00 UTC, the
job can take a few minutes to run.
